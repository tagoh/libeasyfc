/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * ezfc-font-config-private.h
 * Copyright (C) 2011-2018 Akira TAGOH
 * 
 * Authors:
 *   Akira TAGOH  <akira@tagoh.org>
 * 
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __EZFC_FONT_CONFIG_PRIVATE_H__
#define __EZFC_FONT_CONFIG_PRIVATE_H__

#include <glib.h>
#include "ezfc-mem.h"
#include <fontconfig/fontconfig.h>
#include "ezfc-font-config.h"

G_BEGIN_DECLS

typedef enum _ezfc_font_config_type_t {
	EZFC_FONT_CONFIG_TYPE_SYSTEM	= 1 << 0,
	EZFC_FONT_CONFIG_TYPE_USER	= 1 << 1
} ezfc_font_config_type_t;

typedef struct _ezfc_font_config_private_t {
	ezfc_mem_t               parent;
	gchar                   *name;
	gchar                   *desc;
	gboolean                 is_enabled;
	gint                     type;
} ezfc_font_config_private_t;

ezfc_font_config_t *ezfc_font_config_new            (void);
gboolean            ezfc_font_config_set_name       (ezfc_font_config_t      *info,
                                                     const gchar             *name);
gboolean            ezfc_font_config_set_description(ezfc_font_config_t      *info,
                                                     const gchar             *desc);
gboolean            ezfc_font_config_set_enabled    (ezfc_font_config_t      *info,
                                                     gboolean                 is_enabled);
void                ezfc_font_config_set_config_type(ezfc_font_config_t      *info,
						     gint                     type);

G_END_DECLS

#endif /* __EZFC_FONT_CONFIG_PRIVATE_H__ */
