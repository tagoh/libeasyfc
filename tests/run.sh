#! /bin/sh
top_srcdir=$(dirname $0)/..
top_builddir=$(dirname $0)/../build
export GI_TYPELIB_PATH=${top_builddir}/libeasyfc-gobject/.libs:${top_builddir}/libeasyfc-gobject:${top_srcdir}/libeasyfc-gobject/.libs:${top_srcdir}/libeasyfc-gobject/:$GI_TYPELIB_PATH
export LD_LIBRARY_PATH=${top_builddir}/libeasyfc-gobject/.libs:${top_builddir}/libeasyfc-gobject:${top_builddir}/libeasyfc/.libs:${top_builddir}/libeasyfc:${top_srcdir}/libeasyfc-gobject/.libs:${top_srcdir}/libeasyfc-gobject:${top_srcdir}/libeasyfc/.libs:${top_srcdir}/libeasyfc:$LD_LIBRARY_PATH

"$@"

