/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * ezfc-font-config.h
 * Copyright (C) 2011-2018 Akira TAGOH
 * 
 * Authors:
 *   Akira TAGOH  <akira@tagoh.org>
 * 
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#if !defined (__EZFC_H__INSIDE) && !defined (__EZFC_COMPILATION)
#error "Only <libeasyfc/ezfc.h> can be included directly."
#endif

#ifndef __EZFC_FONT_CONFIG_H__
#define __EZFC_FONT_CONFIG_H__

#include <glib.h>

G_BEGIN_DECLS

/**
 * ezfc_font_config_t:
 *
 * All the fields in the <structname>ezfc_font_config_t</structname>
 * structure are private to the #ezfc_font_config_t implementation.
 */
typedef struct _ezfc_font_config_t	ezfc_font_config_t;

ezfc_font_config_t *ezfc_font_config_ref                 (ezfc_font_config_t *info);
void                ezfc_font_config_unref               (ezfc_font_config_t *info);
const gchar        *ezfc_font_config_get_name            (ezfc_font_config_t *info);
const gchar        *ezfc_font_config_get_description     (ezfc_font_config_t *info);
gboolean            ezfc_font_config_is_enabled          (ezfc_font_config_t *info);
gboolean            ezfc_font_config_is_user_conf        (ezfc_font_config_t *info);
gboolean            ezfc_font_config_is_system_conf      (ezfc_font_config_t *info);
gboolean            ezfc_font_config_is_writable         (ezfc_font_config_t *info);
gboolean            ezfc_font_config_set_enable          (ezfc_font_config_t *info,
                                                          gboolean            flag,
                                                          GError             **error);
GList              *ezfc_font_config_get_list            (void);
GList              *ezfc_font_config_get_list_from       (FcConfig           *config);
GList              *ezfc_font_config_get_config_dirs     (void);
GList              *ezfc_font_config_get_config_dirs_from(FcConfig           *config);

G_END_DECLS

#endif /* __EZFC_FONT_CONFIG_H__ */
