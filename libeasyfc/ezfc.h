/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * ezfc.h
 * Copyright (C) 2011-2018 Akira TAGOH
 * 
 * Authors:
 *   Akira TAGOH  <akira@tagoh.org>
 * 
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __EZFC_H__
#define __EZFC_H__

#include <glib.h>

#define __EZFC_H__INSIDE
#include <libeasyfc/ezfc-alias.h>
#include <libeasyfc/ezfc-config.h>
#include <libeasyfc/ezfc-error.h>
#include <libeasyfc/ezfc-font.h>
#include <libeasyfc/ezfc-font-config.h>
#include <libeasyfc/ezfc-utils.h>
#undef __EZFC_H__INSIDE

#endif /* __EZFC_H__ */
